const timeout = duration => new Promise((resolve) => {
    setTimeout(resolve, duration)
});

export default timeout
