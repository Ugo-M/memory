function createGrid(){
    const grid = [];
    for (let i = 0; i < 20; i++){
        grid.push({
            'isVisible' : false,
            'image' : i%10,
            'src': "../../assets/images/dos.jpeg",
            setVisible(isVisible) {
                this.isVisible = isVisible
            },
            display() {
                return this.src = "../../assets/images/image" + this.image + ".jpeg";
            },
            hide() {
                return this.src = "../../assets/images/dos.jpeg";
            }
        });
    }
    return grid;
}

function compareCards(card1, card2){
    return (card1.image === card2.image);
}

export { createGrid, compareCards }