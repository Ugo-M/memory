import {compareCards, createGrid} from './grid.js'
import {shuffle} from "../utils/shuffle.js";

document.addEventListener("DOMContentLoaded",()=>{
    const params = new URLSearchParams(window.location.search);
    let shuffleGrid = params.get('shuffle');
    const grid = shuffleGrid === 'false' ? createGrid() : shuffle(createGrid());
    console.log(grid);
    let previous = {
        'cell': null,
        'card': null
    };
    let row = -1;
    for (let i = 0; i < 20; i++){
        let col = i%4;
        if (i%4 === 0){
            row += 1;
        }
        const cell = document.querySelector("#row_" + row + "-col_" + col);
        cell.addEventListener("click", async () => {
            cell.src = grid[i].display();
            console.log(previous);
            if (previous.card !== null) {
                if (compareCards(grid[i], previous.card)) {
                    previous.cell = null;
                    previous.card = null;
                } else {
                    setTimeout(() => {
                        cell.src = grid[i].hide();
                        previous.cell.src = previous.card.hide();
                        previous.cell = null;
                        previous.card = null;
                    }, 1500);
                }
            }
            else {
                previous.cell = cell;
                previous.card = grid[i];
            }
        })
    }
})