function createGame() {
    return {
        card1: null,
        card2: null,
        reveal(card) {
            card.setVisible(!card.isVisible)
            if (this.card1 == null) {
                this.card1 = card
                this.card1.listener()
                return
            }
            this.card2 = card
            this.card2.listener()
            if (this.card1.image === this.card2.image) {
                return
            }
            setTimeout(() => {
                this.card1.setVisible(false)
                this.card1.listener()
                this.card2.setVisible(false)
                this.card2.listener()
                this.card1 = null
                this.card2 = null
            }, 1000)
        }
    }
}

export { createGame }
