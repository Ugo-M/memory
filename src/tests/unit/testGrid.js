import {expect, assert} from "chai";
import {compareCards, createGrid} from "../../memory/grid.js";
import {shuffle} from "../../utils/shuffle.js";

describe("a memory game", () => {
    it("must have correct dimension", () => {
        const grid = createGrid();
        expect(grid.length).to.equal(20);
    });

    it("must have cells", () => {
        const grid = createGrid();
        const topLeft = grid[4];
        expect(topLeft.isVisible).to.equal(false);
        expect(topLeft.setVisible).to.be.an.instanceof(Function)
        topLeft.setVisible(true)
        expect(topLeft.isVisible).to.equal(true)
        expect(topLeft.image).to.satisfy((n) => 0 <= n < 10)
    });

    it("must shuffle grid", () => {
        const grid = createGrid();

        // shuffle 3 times to minimize chances of getting the same grid
        assert.isTrue(
            grid.entries() !== shuffle(grid).entries() &&
            grid.entries() !== shuffle(grid).entries() &&
            grid.entries() !== shuffle(grid).entries()
        );
    });

    it("must check if two cards are the same", () => {
        const grid = createGrid();
        expect(compareCards(grid[0], grid[10])).to.be.true;
        expect(compareCards(grid[0], grid[8])).to.be.false;
    });

    it("must display a card", () => {
        const grid = createGrid();
        grid[0].display();
        expect(grid[0].src).to.equal("../../assets/images/image" + grid[0].image + ".jpeg");
    });

    it("must hide a card", () => {
        const grid = createGrid();
        grid[0].display();
        grid[0].hide();
        expect(grid[0].src).to.equal("../../assets/images/dos.jpeg");
    });
})