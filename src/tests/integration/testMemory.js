import { createGame } from '../../memory/game.js'
import { createGrid } from '../../memory/grid.js'
import chai from 'chai'
const expect = chai.expect
import timeout from '../../utils/timeout.js'
import sinon from 'sinon'

describe("a memory game",()=>{

    it("must keep two matching cards",function (done) {

        this.timeout(5000);
        const grid = createGrid();
        const game = createGame(grid);
        const card1 = grid[0];
        const card2 = grid[10];
        expect(card1.isVisible).to.equal(false);
        expect(card2.isVisible).to.equal(false);
        card1.listener = sinon.fake();
        card2.listener = sinon.fake();
        game.reveal(card1);
        expect(card1.isVisible).to.equal(true);
        game.reveal(card2);
        expect(card2.isVisible).to.equal(true);
        setTimeout(() => {
            expect(card1.isVisible).to.equal(true);
            expect(card2.isVisible).to.equal(true);
            expect(card1.listener.callCount).to.equal(1);
            expect(card2.listener.callCount).to.equal(1);
            done();
        }, 2000);
    })

    it("must cancel two un-matching cards",async function (){

        this.timeout(5000);
        const grid = createGrid();
        const controller = createGame();
        const card1 = grid[0];
        const card2 = grid[16];
        expect(card1.isVisible).to.equal(false);
        expect(card2.isVisible).to.equal(false);
        card1.listener = ()=>{};
        card2.listener = ()=>{};
        controller.reveal(card1);
        expect(card1.isVisible).to.equal(true);
        controller.reveal(card2);
        expect(card2.isVisible).to.equal(true);
        await timeout(2000);
        expect(card1.isVisible).to.equal(false);
        expect(card2.isVisible).to.equal(false);
    })
})
